include docker/.docker.conf
export $(shell sed 's/=.*//' docker/.docker.conf)

DOCKER_COMPOSE=eval ${DOCKER_COMPOSE_CMD}

##########################
###### DEV DOCKER ########
##########################
up:
	${DOCKER_COMPOSE} up --build -d
logs:
	${DOCKER_COMPOSE} logs -tf
stop:
	${DOCKER_COMPOSE} stop
ps:
	${DOCKER_COMPOSE} ps
purge:
	${DOCKER_COMPOSE} down --remove-orphans -v
	${DOCKER_COMPOSE} rm -f -v
restart:
	@make purge
	@make up
exec:
	${DOCKER_COMPOSE} exec -u dev apache bash
exec-root:
	${DOCKER_COMPOSE} exec apache bash