# GAC TECHNOLOGY - test
 
## Installation

### Via Docker

(Prérequis : docker, docker-compose, make)

- Cloner le projet : `git clone [url] gac-test`
- Démarrage des dockers : `make up`

### Via serveur Apache

- Cloner le projet : `git clone [url] gac-test`
- Ajouter un virtualHost sur le serveur Apache, pointant sur le projet

## Initialisation

- Si hors docker, ajouter un fichier .env.local avec les informations pour la base de données  (DATABASE_URL=mysql://USER:PASSWORd@HOST:3306/DATABASE)

Les commandes sont à faire dans la console (pour docker : `make exec`)

- `composer install`
- `php bin/console doctrine:migration:migrate`
- Rendez vous sur http://localhost/api/doc

## Arrêt des dockers

- `make purge`