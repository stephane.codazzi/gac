<?php

namespace App\Controller;

use App\Entity\CallTicket;
use App\Repository\CallTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class CallController extends AbstractController
{
    /**
     * Calls importation
     *
     * @Route("/api/calls/import", methods={"POST"})
     * @SWG\Response(
     *     response=201,
     *     description="Returns errors list"
     * )
     * consumes={"multipart/form-data"},
     * @SWG\Parameter(
     *     name="file",
     *     in="formData",
     *     type="file",
     *     description="CSV file"
     * )
     * @SWG\Tag(name="call")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $errors = [];

        $content =  $request->getContent();
        $dir = $this->getParameter('kernel.project_dir') . '/var/';
        $fileName = uniqid() . '.csv';
        $content = preg_replace("/^(.*\n){7}/", "", $content);
        file_put_contents($dir . $fileName, $content);
        $row = 0;

        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        if (($handle = fopen($dir . $fileName, "r")) !== false) {
            while (($data = fgetcsv($handle, 10000, ";")) !== false) {
                $row++;
                if (count($data) < 7) {
                    continue;
                }

                $call = new CallTicket();
                $call->setAccount($data[0]);
                $call->setInvoice($data[1]);
                $call->setSubscriber($data[2]);
                $date = \DateTime::createFromFormat('d/m/Y H:i:s', $data[3] . ' ' . $data[4]);

                if ($date === false) {
                    $errors[] = 'L' . $row . ': bad date';
                    continue;
                }

                $call->setDate($date);
                if (!empty($data[5])) {
                    if (preg_match("/^[0-6]{2}:[0-6]{2}:[0-6]{2}$/", $data[5])) {
                        $hour = intval(substr($data[5], 0, 2));
                        $min = intval(substr($data[5], 3, 2));
                        $sec = intval(substr($data[5], 5, 2));
                        $call->setTimeReal($sec + (60 * $min) + (3600 * $hour));
                    } else {
                        $call->setDataReal(intval($data[5]));
                    }
                }

                if (!empty($data[6])) {
                    if (preg_match("/^[0-6]{2}:[0-6]{2}:[0-6]{2}$/", $data[6])) {
                        $hour = intval(substr($data[5], 0, 2));
                        $min = intval(substr($data[5], 3, 2));
                        $sec = intval(substr($data[5], 5, 2));
                        $call->setTimeInvoiced($sec + (60 * $min) + (3600 * $hour));
                    } else {
                        $call->setDataInvoiced(intval($data[6]));
                    }
                }
                $call->setType(utf8_encode($data[7]));
                $em->persist($call);

                if ($row % 500 === 0) {
                    $em->flush();
                    $em->clear();
                }
            }
            fclose($handle);
        }

        $em->flush();
        $em->clear();

        unlink($dir . $fileName);

        return $this->json([
            'errors' => $errors
        ]);
    }

    /**
     * get call duration after 15/02/2012 (included)
     *
     * @Route("/api/calls/getCallTimeAfterFeb", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns call duration in secondes"
     * )
     * @SWG\Tag(name="call")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCallTimeAfterFeb(Request $request, EntityManagerInterface $em)
    {
        /** @var CallTicketRepository $callrepository */
        $callRepository = $em->getRepository(CallTicket::class);
        return $this->json([
            'duration' => $callRepository->getDurationAfterDate(new \DateTime('2010-02-15T00:00:00'))
        ]);
    }

    /**
     * get top 10
     *
     * @Route("/api/calls/getTop10", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns top 10"
     * )
     * @SWG\Tag(name="call")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function getTop10(Request $request, EntityManagerInterface $em)
    {
        /** @var CallTicketRepository $callrepository */
        $callRepository = $em->getRepository(CallTicket::class);

        return $this->json([
            'duration' => $callRepository->getTop10()
        ]);
    }

    /**
     * get TotalSms
     *
     * @Route("/api/calls/getTotalSms", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns total SMS"
     * )
     * @SWG\Tag(name="call")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function getTotalSms(Request $request, EntityManagerInterface $em)
    {
        /** @var CallTicketRepository $callrepository */
        $callRepository = $em->getRepository(CallTicket::class);

        return $this->json([
            'duration' => $callRepository->getTotalSms()
        ]);
    }
}
