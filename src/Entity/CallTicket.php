<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CallTicketRepository")
 */
class CallTicket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $invoice;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $subscriber;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeReal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dataReal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeInvoiced;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dataInvoiced;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getInvoice(): ?string
    {
        return $this->invoice;
    }

    public function setInvoice(string $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getSubscriber(): ?string
    {
        return $this->subscriber;
    }

    public function setSubscriber(string $subscriber): self
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTimeReal(): ?int
    {
        return $this->timeReal;
    }

    public function setTimeReal(?int $timeReal): self
    {
        $this->timeReal = $timeReal;

        return $this;
    }

    public function getDataReal(): ?int
    {
        return $this->dataReal;
    }

    public function setDataReal(?int $dataReal): self
    {
        $this->dataReal = $dataReal;

        return $this;
    }

    public function getTimeInvoiced(): ?int
    {
        return $this->timeInvoiced;
    }

    public function setTimeInvoiced(?int $timeInvoiced): self
    {
        $this->timeInvoiced = $timeInvoiced;

        return $this;
    }

    public function getDataInvoiced(): ?int
    {
        return $this->dataInvoiced;
    }

    public function setDataInvoiced(?int $dataInvoiced): self
    {
        $this->dataInvoiced = $dataInvoiced;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
