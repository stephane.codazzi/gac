<?php

namespace App\Repository;

use App\Entity\CallTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Call|null find($id, $lockMode = null, $lockVersion = null)
 * @method Call|null findOneBy(array $criteria, array $orderBy = null)
 * @method Call[]    findAll()
 * @method Call[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CallTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CallTicket::class);
    }

    public function getDurationAfterDate($date)
    {
        return $this->createQueryBuilder('c')
            ->select('SUM(c.timeReal)')
            ->andWhere('c.timeReal IS NOT NULL')
            ->andWhere('c.date > :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function getTop10()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('(HOUR(c.date) < :date8 OR HOUR(c.date) > :date18)')
            ->setParameter('date8', 8)
            ->setParameter('date18', 18)
            ->setMaxResults(10)
            ->orderBy('c.dataInvoiced', 'DESC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY)
            ;
    }

    public function getTotalSms()
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->andWhere('c.type LIKE :type')
            ->setParameter('type', '%sms%')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    // /**
    //  * @return Call[] Returns an array of Call objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Call
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
